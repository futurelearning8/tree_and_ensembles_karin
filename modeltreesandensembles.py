# 1.a. MSE on Validation Set is: 0.003421120983624694
# 1.b. Best Validation Score when d=10, MSE:0.0030031941091234475
# 2.c MSE on Validation Set with Begging is: 0.0023761847470375017
# 3.b. MSE on Validation Set with Random Forest is: 0.002132566230311197
# 3.c Feature Importance by Random Forest is: [0.01828466 0.90489073 0.01685341 0.0599712 ]
# 4.b. MSE on Validation Set with Boosting is: 0.0024218182601402644
# 5.b. MSE on Training Set with Stacking using Linear Regression is: 0.0036575587483827195
# 5.b. MSE on Validation Set with Stacking using Linear Regression is: 0.00365269891061487
# 5.b. MSE on Training Set with Stacking using SVR is: 0.0031258123575573183
# 5.b. MSE on Validation Set with Stacking using SVR is: 0.003269441128988967
# 5.b. MSE on Training Set with Stacking using KNN is: 0.0016852709971614597
# 5.b. MSE on Validation Set with Stacking using KNN is: 0.0026126254720835657
# 5.b. MSE on Training Set with Stacking using Random Forest is: 0.00027731039332890066
# 5.b. MSE on Validation Set with Stacking using Random Forest is: 0.0021025680506481697
# 5.c. MSE on Validation Set with Stacking is: 0.002051641841801481

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn import tree
from sklearn.tree import DecisionTreeRegressor
from parameters import TEST_SIZE
import os.path
from scipy import stats
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import BaggingRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsRegressor


def check_data_path(data_path):
    if not os.path.exists(data_path):
        print("INPUT FILE WAS NOR FOUND!")
        exit()


def preprocess_step():
    FILE_PATH = "../data/ccpp.xlsx"
    check_data_path(FILE_PATH)
    df = pd.read_excel(FILE_PATH)
    print(f'Size of weather data frame is: {df.shape}\n')

    z = np.abs(stats.zscore(df._get_numeric_data()))
    df = df[(z < 3).all(axis=1)]

    scaler = preprocessing.MinMaxScaler()
    scaler.fit(df)
    normalized_df = pd.DataFrame(scaler.transform(df), index=df.index, columns=df.columns)

    return normalized_df


def find_valuation_best_depth(X_train, X_val, y_train, y_val):
    best_val_d = 0
    best_val_mse = np.inf
    best_val_dt = None
    for d in range(1, 20):
        dec_train_regressor = DecisionTreeRegressor(random_state=0, max_depth=d)
        dec_train_regressor.fit(X_train, y_train)

        y_pred_val = dec_train_regressor.predict(X_val)
        val_mse = mean_squared_error(y_val, y_pred_val)
        print(f'MSE on Validation Set is with depth {d}: {mean_squared_error(y_val, y_pred_val)}')

        if val_mse < best_val_mse:
            best_val_mse = val_mse
            best_val_d = d
            best_val_dt = dec_train_regressor

    return best_val_d, best_val_mse, best_val_dt


def main():
    df = preprocess_step()

    # 2. Dataset split
    X = df[df.columns.difference(['PE'])]
    y = df.filter(like='PE')

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=TEST_SIZE, random_state=42, shuffle=True)

    # 1. Decision Tree
    dec_train_regressor = DecisionTreeRegressor(random_state=0)
    dec_train_regressor.fit(X_train, y_train)

    y_pred_val = dec_train_regressor.predict(X_val)
    print(f'1.a. MSE on Validation Set is: {mean_squared_error(y_val, y_pred_val)}\n')

    print("1.b. Find Best depth for Validation set...")
    best_val_d, best_val_mse, best_val_dec_train_reg = find_valuation_best_depth(X_train, X_val, y_train, y_val)
    print(f'Best Validation Score when d={best_val_d}, MSE:{best_val_mse}\n')

    # 1.c.
    tree.export_graphviz(best_val_dec_train_reg, out_file="dec_tree_best_val_depth_MSE.dot")
    # dot -Tpng dec_tree_best_val_depth_MSE.dot -o dec_tree_best_val_depth_MSE.png

    # 2. Bagging - a.b.c.
    begg_dt_regr = BaggingRegressor(base_estimator=DecisionTreeRegressor(random_state=0,  max_depth=best_val_d + 3),
                                    n_estimators=10, random_state=0)
    begg_dt_regr.fit(X_train, y_train.values.ravel())
    y_pred_val_begg = begg_dt_regr.predict(X_val)
    print(f'2.c. MSE on Validation Set with Begging is: {mean_squared_error(y_val, y_pred_val_begg)}\n')

    # 3. Random Forest
    rf_dt_regr = RandomForestRegressor(n_estimators=100, max_depth=best_val_d + 4, random_state=0)
    rf_dt_regr.fit(X_train, y_train.values.ravel())
    y_pred_val_rf = rf_dt_regr.predict(X_val)
    print(f'3.b. MSE on Validation Set with Random Forest is: {mean_squared_error(y_val, y_pred_val_rf)}')
    print(f'3.c. Feature Importance by Random Forest is: {rf_dt_regr.feature_importances_}\n')

    # 4. Boosting
    boost_dt_regr = GradientBoostingRegressor(random_state=0, max_depth=best_val_d, validation_fraction=0.2,
                                              n_iter_no_change=10, tol=0.001)
    boost_dt_regr.fit(X_train, y_train.values.ravel())
    y_pred_val_boost = boost_dt_regr.predict(X_val)
    boost_dt_regr.score(X_val, y_val.values.ravel())
    print(f'4.b. MSE on Validation Set with Boosting is: {mean_squared_error(y_val, y_pred_val_boost)}\n')

    # 5. Stacking
    # 5.a.
    linear_reg = LinearRegression()
    svr_regr = SVR()
    knn_regr = KNeighborsRegressor()
    rf_regr = RandomForestRegressor(random_state=0)

    for model in (linear_reg, svr_regr, knn_regr, rf_regr):
        model.fit(X_train, y_train.values.ravel())

    # 5.b.
    regressions_names = ("Linear Regression", "SVR", "KNN", "Random Forest")
    y_train_pred_lst = []
    x_val_pred_lst = []
    for model, regr_name in zip((linear_reg, svr_regr, knn_regr, rf_regr), regressions_names):
        y_train_pred = model.predict(X_train)
        y_train_pred_lst.append(y_train_pred)
        print(f'5.b. MSE on Training Set using {regr_name} is: '
              f'{mean_squared_error(y_train, y_train_pred)}')
        y_val_pred = model.predict(X_val)
        x_val_pred_lst.append(y_val_pred)
        print(f'5.b. MSE on Validation Set using {regr_name} is: '
              f'{mean_squared_error(y_val, y_val_pred)}')

    for y_train_pred, regr_name in zip(y_train_pred_lst, regressions_names):
        X_train.loc[:, regr_name] = y_train_pred

    for y_val_pred, regr_name in zip(x_val_pred_lst, regressions_names):
        X_val.loc[:, regr_name] = y_val_pred

    combiner = KNeighborsRegressor()
    combiner.fit(X_train, y_train)
    y_pred_val_combiner = combiner.predict(X_val)
    print(f'5.c. MSE on Validation Set with Stacking is: {mean_squared_error(y_val, y_pred_val_combiner)}')


if __name__ == '__main__':
    main()
