# XGBoost.3. MSE on Validation Set with Stacking is: 0.001784189831493836
# XGBoost-sklearn.3. MSE on Validation Set with Stacking is: 0.001784189831493836

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from parameters import TEST_SIZE
import os.path
from scipy import stats
import xgboost as xgb
from sklearn.metrics import mean_squared_error
from xgboost.sklearn import XGBRegressor


def check_data_path(data_path):
    if not os.path.exists(data_path):
        print("INPUT FILE WAS NOR FOUND!")
        exit()


def preprocess_step():
    FILE_PATH = "../data/ccpp.xlsx"
    check_data_path(FILE_PATH)
    df = pd.read_excel(FILE_PATH)
    print(f'Size of weather data frame is: {df.shape}\n')

    z = np.abs(stats.zscore(df._get_numeric_data()))
    df = df[(z < 3).all(axis=1)]

    scaler = preprocessing.MinMaxScaler()
    scaler.fit(df)
    normalized_df = pd.DataFrame(scaler.transform(df), index=df.index, columns=df.columns)

    return normalized_df


def main():
    df = preprocess_step()

    # 2. Dataset split
    X = df[df.columns.difference(['PE'])]
    y = df.filter(like='PE')

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=TEST_SIZE, random_state=42, shuffle=True)

    params = {'n_estimators': 1000, 'max_depth': 7, 'eta': 0.1, 'subsample': 0.7, 'colsample_bytree': 0.8}
    # Run XGB
    xgb_regressor = xgb.XGBRegressor(**params)
    xgb_regressor.fit(X_train, y_train)
    y_pred_val_xgb = xgb_regressor.predict(X_val)
    print(f'XGBoost.3. MSE on Validation Set with Stacking is: {mean_squared_error(y_val, y_pred_val_xgb)}')

    # Run XGB
    xgb_regressor = XGBRegressor(**params)
    xgb_regressor.fit(X_train, y_train)
    y_pred_val_xgb = xgb_regressor.predict(X_val)
    print(f'XGBoost-sklearn.3. MSE on Validation Set with Stacking is: {mean_squared_error(y_val, y_pred_val_xgb)}')


if __name__ == '__main__':
    main()
